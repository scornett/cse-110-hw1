package edu.ucsd.cse110.library;

import java.time.LocalDate;
import java.util.*;

public class Library {
	//Member members = new Member("chris", student);
	//public List<Member> members = new List<Member>();
	private List<Member> members;
	
	//Publication publications = new Book("Call of the Wild");
	private List<Publication> publications; // = new ArrayList<Publication>();

	private PubAdmin pubAdmin = new PubAdmin();

	public Library(){
		publications = new ArrayList<Publication>();
		members = new ArrayList<Member>();
		pubAdmin = new PubAdmin();
	}
	
	public List<Member> getMembers(){
		return members;
	}
	public List<Publication> getPublications(){
		return publications;
	}
	public void checkout(){
		
	}
	
	public void returnPub(){}
	
	/*
	public void doCheckout(){
		pubAdmin.checkoutOublication(new Member(), new Publication());
	}
	*/
	

	//members.add("Scott");
	
	//Has other components to use
	//Rules Object into Enterprise
	//Factory making Enterprise
	//Library has a facade
	//Facade has mediator and four functions and Rule Objects

	//Library is a concrete class, 
	//Component
	//RuleObjectFactory
	//FunctionMediator
}
