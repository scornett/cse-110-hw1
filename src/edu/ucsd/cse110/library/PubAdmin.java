package edu.ucsd.cse110.library;

import java.time.LocalDate;
//enterprise component with rule objects
public class PubAdmin {

	public PubAdmin () {}

	public CheckoutMediator cMed = new CheckoutMediator();
	public FeeMediator fMed = new FeeMediator();

	public void checkoutPublication(Member m, Publication p) {
		cMed.checkoutPub(m, p);
	}

	public void returnPublication(Publication p) {
		cMed.returnPub(p);
	}

	public double getFee(Member m) {
		return fMed.getFee(m);
	}

	public boolean hasFee(Member m){
		return fMed.hasFee(m);
	}


}
