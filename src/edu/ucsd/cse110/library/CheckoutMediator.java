package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class CheckoutMediator {

	public CheckoutMediator() {}

	public void checkoutPub(Member m, Publication p) {
		if (!p.isCheckout()){
			//LocalDate l;
			p.checkout(m, p.getCheckoutDate());
		}
		else {
			System.out.println("Not available");
		}
	
	}

	public void returnPub(Publication p) {
		p.pubReturn(p.getCheckoutDate());
	}

}
