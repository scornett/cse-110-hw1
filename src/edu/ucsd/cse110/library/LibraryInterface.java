package edu.ucsd.cse110.library;

import java.time.LocalDate;
//facade
public interface LibraryInterface {

	public void checkoutPublication(Member m, Publication p);
	public void returnPublication(Publication p);
	public double getFee(Member m);
	public boolean hasFee(Member m);

}
