package edu.ucsd.cse110.library.rules;


public interface Action {
	public void execute(Properties prop);
	public String getErrors();
}
